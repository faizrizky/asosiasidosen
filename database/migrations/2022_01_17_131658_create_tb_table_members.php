<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbTableMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_table_members', function (Blueprint $table) {
            $table->id();
            $table->string('id')->nullable();
            $table->string('image')->nullable();
            $table->string('kta')->nullable();
            $table->string('kta_url')->nullable();
            $table->string('name')->nullable();
            $table->string('name_url')->nullable();
            $table->string('instansi')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_table_members');
    }
}
