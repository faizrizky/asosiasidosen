<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbTableHome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_table_home', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->string('description')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('email')->nullable();
            $table->string('title_about')->nullable();
            $table->string('subtitle_about')->nullable();
            $table->string('title_team')->nullable();
            $table->string('subtitle_team')->nullable();
            $table->string('title_member')->nullable();
            $table->string('subtitle_members')->nullable();
            $table->string('title_agenda')->nullable();
            $table->string('subtitle_agenda')->nullable();
            $table->string('title_gallery')->nullable();
            $table->string('subtitle_gallery')->nullable();
            $table->string('title_registration')->nullable();
            $table->string('subtitle_registration')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_table_home');
    }
}
