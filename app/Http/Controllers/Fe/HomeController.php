<?php

namespace App\Http\Controllers\Fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HomeModel;
use App\Models\AboutModel;
use App\Models\AgendaModel;
use App\Models\GalleryModel;
use App\Models\RegistrationModel;
use App\Models\TeamModel;
use App\Models\MembersModel;

use DB;
use File;
use Input;
use Mail;

class HomeController extends Controller
{
    private $active = [
        'upload_path' => 'app/public/home',
        'download_path' => 'storage/home',
        'agenda_path' => 'app/public/agenda',
        'gallery_path' => 'app/public/gallery',
        'team_path' => 'app/public/team',
        'members_path' => 'app/public/members',
    ];

    function __construct(){
        $this->active['url'] = url('');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        $home = HomeModel::first();
        $about = AboutModel::all();
        $team = TeamModel::all()->chunk(2);
        $agenda = AgendaModel::orderBy('id', 'DESC')->limit(3)->get();
        $gallery = GalleryModel::all();
        $members = MembersModel::all();
        $registration = RegistrationModel::first();
        $data = compact('active',
                        'home',
                        'about',
                        'team',
                        'members',
                        'agenda',
                        'gallery',
                        'registration'
                    );
        return view('frontend.home.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = ['g-recaptcha-response' => 'required|captcha'];
        $rule_msg = [
            'g-recaptcha-response' => [
                'required' => 'Please verify that you are not a robot.',
                'captcha' => 'Captcha error! try again later or contact site admin.',
            ],
        ];

        $data = $request->all();
        $id = isset($data['id']) ? $data['id']:0;

        if ($request->hasFile('file')) {
            $rule['file'] = 'required|mimes:pdf,doc,docx,zip|max:5120';
            $rule_msg['mimes'] = 'please make sure the file is doc, docx or pdf';
        }
        $validator = \Validator::make($request->all(), $rule, $rule_msg);
        $msg = "";
        try {
            $validator->validate();
            if ($request->hasFile('file')) {
                $data['file'] = \Myhelper::upload_file($request, $this->active['upload_path'],'file','old_file');
            }

            $orders = OrdersModel::updateOrCreate(
                ['id' => $id],
            $data);

            $msg = "Operation success";
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }

        if ($validator->fails()) {
            $error = $validator->errors()->all();
            if (strpos($error[0], "failed to upload") !== false) {
            }else{
                return redirect($this->active['url']."#contact")->withErrors($error);
            }
        }
        \Session::flash('msg', $msg);
        return redirect($this->active['url']."#contact");
    }

}
