<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutModel;

use DB;
use File;
use Input;

class AboutController extends Controller
{
    private $active = ['about'=>'active', 'title' => 'Tentang Kami'];

    function __construct(){
        $this->active['url'] = url('adminpanel/about');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        $data = AboutModel::all();
        return view('backend.about.index', compact('active', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $active = $this->active;
        return view('backend.about.form', compact('active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [];
        $rule_msg = [];

        $data = $request->all();
        $id = isset($data['id']) ? $data['id']:0;
        $validator = \Validator::make($request->all(), $rule);
        $msg = "";
        try {
            $about = AboutModel::updateOrCreate(
                ['id' => $id],
            $data);
            $msg = "Operation success";
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }

        if ($validator->fails()) {
            $error = $validator->errors()->all();
            \Session::flash('error', $error);
        }
        \Session::flash('msg', $msg);
        return redirect($this->active['url']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $active = $this->active;
        $data = AboutModel::findOrFail($id);
        return view('backend.about.form', compact('active', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AboutModel::findOrFail($id)->delete();
        \Session::flash('msg', "Operation success");
        return redirect($this->active['url']);
    }
}