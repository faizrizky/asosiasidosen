<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegistrationModel;

use DB;
use File;
use Input;

class RegistrationController extends Controller
{
    private $active = ['registration'=>'active', 'title' => 'Pendaftaran'];

    function __construct(){
        $this->active['url'] = url('adminpanel/registration');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        $data = RegistrationModel::first();
        return view('backend.registration.index', compact('active', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
       
        $msg = "";
        try {
            $setting = RegistrationModel::updateOrCreate(
                ['id' => 1],
            $data);
            $msg = "Operation success";
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }
        \Session::flash('msg', $msg);
        return redirect($this->active['url']);
    }

}
