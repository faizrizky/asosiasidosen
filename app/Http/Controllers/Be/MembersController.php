<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MembersModel;

use DB;
use File;
use Input;

class MembersController extends Controller
{
    private $active = ['members'=>'active', 'title' => 'members','upload_path' => 'app/public/members',
        'download_path' => 'app/public/members'];

    function __construct(){
        $this->active['url'] = url('adminpanel/members');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        $data = MembersModel::orderBy('name')->get();
        return view('backend.members.index', compact('active', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $active = $this->active;
        return view('backend.members.form', compact('active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [];
        $rule_msg = [];

        $data = $request->all();
        $id = isset($data['id']) ? $data['id']:0;

        if ($request->hasFile('image')) {
            $rule['image'] = 'mimes:jpeg,jpg,png|max:3072';
            $rule_msg['mimes'] = 'please make sure the file is jpg, jpeg or png';
        }
        $validator = \Validator::make($request->all(), $rule, $rule_msg);
        $msg = "";
        try {
            $validator->validate();
            if ($request->hasFile('image')) {
                $data['image'] = \Myhelper::upload_file($request, $this->active['upload_path'],'image','old_image');
            }

            $members = MembersModel::updateOrCreate(
                ['id' => $id],
            $data);
            $msg = "Operation success";
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }

        if ($validator->fails()) {
            $error = $validator->errors()->all();
            if (strpos($error[0], "failed to upload") !== false) {
            }else{
                return redirect($this->active['url'])->withErrors($error);
            }
        }
        \Session::flash('msg', $msg);
        return redirect($this->active['url']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $active = $this->active;
        $data = MembersModel::findOrFail($id);
        return view('backend.members.form', compact('active', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MembersModel::findOrFail($id);
        \Myhelper::delete_file($this->active['upload_path'],$data->image);
        $data->delete();
        \Session::flash('msg', "Operation success");
        return redirect($this->active['url']);
    }
}
