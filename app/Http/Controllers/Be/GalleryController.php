<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GalleryModel;

use DB;
use File;
use Input;

class GalleryController extends Controller
{
    private $active = ['gallery'=>'active', 'title' => 'galeri ','upload_path' => 'app/public/gallery',
        'download_path' => 'app/public/gallery'];

    function __construct(){
        $this->active['url'] = url('adminpanel/gallery');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        $data = GalleryModel::orderBy('title')->get();
        return view('backend.gallery.index', compact('active', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $active = $this->active;
        return view('backend.gallery.form', compact('active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [];
        $rule_msg = [];

        $data = $request->all();
        $id = isset($data['id']) ? $data['id']:0;

        if ($request->hasFile('image')) {
            $rule['image'] = 'mimes:jpeg,jpg,png|max:3072';
            $rule_msg['mimes'] = 'please make sure the file is jpg, jpeg or png';
        }
        if ($request->hasFile('file')) {
            $rule['file'] = 'mimes:pdf|max:3072';
            $rule_msg['mimes'] = 'please make sure the file is pdf';
        }
        $validator = \Validator::make($request->all(), $rule, $rule_msg);
        $msg = "";
        try {
            $validator->validate();
            if ($request->hasFile('image')) {
                $data['image'] = \Myhelper::upload_file($request, $this->active['upload_path'],'image','old_image');
            }
            if ($request->hasFile('file')) {
                $data['file'] = \Myhelper::upload_file($request, $this->active['upload_path'],'file','old_file');
            }

            $gallery = GalleryModel::updateOrCreate(
                ['id' => $id],
            $data);
            $msg = "Operation success";
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }

        if ($validator->fails()) {
            $error = $validator->errors()->all();
            if (strpos($error[0], "failed to upload") !== false) {
            }else{
                return redirect($this->active['url']."#contact")->withErrors($error);
            }
        }
        \Session::flash('msg', $msg);
        return redirect($this->active['url']."#contact");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $active = $this->active;
        $data = GalleryModel::findOrFail($id);
        return view('backend.gallery.form', compact('active', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = GalleryModel::findOrFail($id);
        \Myhelper::delete_file($this->active['upload_path'],$data->image);
        $data->delete();
        \Session::flash('msg', "Operation success");
        return redirect($this->active['url']);
    }
}
