<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

use DB;
use File;
use Input;

class PasswordController extends Controller
{
    private $active = ['change-password'=>'active', 'title' => 'change password'];

    function __construct(){
        $this->active['url'] = url('adminpanel/change-password');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        return view('backend.password.index', compact('active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $rule['password'] ='min:6|required|confirmed';
        $rule_msg['confirmed'] = 'Password and password confirmation must match';
        $validator = \Validator::make($request->all(), $rule, $rule_msg);
        $msg = "";
        $alert = "success";
        try {
            $validator->validate();

            $user = \Auth::user();
            if (password_verify($data['old_password'], $user->getAuthPassword())) {
                $user_id = \Auth::User()->id;                       
                $user = User::find($user_id);
                $user->password = bcrypt($data['password']);
                $user->save(); 
                $msg = "Operation success";
            }else{
                $msg = "Wrong old password";
                $alert = "danger";
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }
        
        if ($validator->fails()) {
            $error = $validator->errors()->all();
            return redirect($this->active['url'])->withErrors($error);
        }else{
            \Session::flash('msg', $msg);
            \Session::flash('alert', $alert);
            return redirect($this->active['url']);
        }
    }

}
