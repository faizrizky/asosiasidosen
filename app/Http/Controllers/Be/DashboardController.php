<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HomeModel;

use DB;
use File;
use Input;

class DashboardController extends Controller
{
    private $active = ['dashboard'=>'active', 'title' => 'dashboard'];

    function __construct(){
        $this->active['url'] = url('adminpanel/dashboard');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        $data = HomeModel::first();
        return view('backend.dashboard.index', compact('active', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
       
        $msg = "";
        try {
            $setting = HomeModel::updateOrCreate(
                ['id' => 1],
            $data);
            $msg = "Operation success";
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }
        \Session::flash('msg', $msg);
        return redirect($this->active['url']);
    }

}
