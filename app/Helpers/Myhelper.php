<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Input;
use Legierski\AES\AES;
use File;
use Carbon\Carbon;

class Myhelper {

	// use after submitting to give message and redirect to specified url
    public static function refresh_page($message = "", $url = "")
    {
        return $message." <i class='fa fa-refresh fa-spin'></i>"."<script>setTimeout(function(){window.location='$url" . "?" . time() . "'}, 2000)</script>";
    }

    // flash message
    public static function message($message = "", $alert = "success")
    {
        \Session::flash('msg', $message);
        \Session::flash('alert', $alert);
    }

    public static function xss_clean($string)
    {
        $data = "";
        // dump($string);
        if (is_array($string)) {
            $data = array_map(function($string)
            {

                if (is_array($string)) {
                    $arr2 = array_map(function($string)
                        {
                            if (is_array($string)) {

                            }else{
                                return Self::str_clean($string,3);
                            }
                        }, $string);
                    return $arr2;
                }else{
                    return Self::str_clean($string,3);
                }
            }, $string);
        }else{
            $data = Self::str_clean($string,3);
        }
        return $data;
    }

    public static function upload_file($request, $path, $name, $old = "old_file", $custom_name = "", $static_name = false, $index = null)
    {
        Self::createDir($path);

        if($index){
            $file = Input::file($name);
            $file = $file[$index-1];
        } else {
            $file = $request->file($name);
        }
        if ($request->input($old)) {
            Self::delete_file($path,$request->input($old));
        }
        $name = $custom_name ? $custom_name:$name;
        if (!$static_name) {
            $name = $name."-".time();
        }
        $filename = $name.".".$file->guessExtension();
        $path = public_path($path);
        $file->move($path,$filename);
        return $filename;
    }

    public static function createDir($path)
    {
        $path = public_path($path);
        if (!File::exists($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }

    public static function delete_file($path, $filename)
    {
        $path_file = public_path($path.'/'.$filename);
        if ($filename && File::exists($path_file)) {
            unlink($path_file);
        }
    }
}