<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\HomeModel;
use App\Models\OrdersModel;
use App\Mail\OrderEmail;

use Mail;

class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmail:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = OrdersModel::whereNull('sent_email')->limit(5)->get();
        $home = HomeModel::first();
        foreach ($data as $key) {
            Mail::to($home->email)->send(new OrderEmail($key));
            $key->sent_email = 1;
            $key->save();
        }
    }
}
