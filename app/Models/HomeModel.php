<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_home";

    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'image1',
        'image2',
        'image3',
        'address',
        'phone',
        'whatsapp',
        'email',
        'title_about',
        'subtitle_about',
        'title_team',
        'subtitle_team',
        'title_members',
        'subtitle_members',
        'title_agenda',
        'subtitle_agenda',
        'title_gallery',
        'subtitle_gallery',
        'title_registration',
        'subtitle_registration',
    ];}
