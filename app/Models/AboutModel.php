<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_about";

    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'icon',
    ];}
