<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembersModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_members";

    protected $fillable = [
        'id',
        'image',
        'kta',
        'kta_url',
        'name',
        'name_url',
        'instansi'
    ];
}
