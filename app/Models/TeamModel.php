<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_team";

    protected $fillable = [
        'name',
        'image',
        'title',
        'description',
    ];}
