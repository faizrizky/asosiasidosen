<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgendaModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_agenda";

    protected $fillable = [
        'title',
        'image',
        'file',
        'description',
    ];}
