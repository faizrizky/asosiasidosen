<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;
use App\Models\HomeModel;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $data = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $author = HomeModel::first();
        return $this->from(env('MAIL_FROM_ADDRESS'))
                   ->view('email')
                   ->with(
                    [
                        'name' => $this->data->name,
                        'email' => $this->data->email,
                        'phone' => $this->data->phone,
                        'subject' => $this->data->subject,
                        'package' => $this->data->pricing->title,
                        'message_email' => $this->data->message,
                        'author_address' => $author->address,
                        'author_phone' => $author->phone,
                        'created_at' => Carbon::parse($this->data->created_at)->format('d-m-Y H:i'),
                    ])
                    ->attach(public_path('app/public/home/'.$this->data->file));
    }
}
