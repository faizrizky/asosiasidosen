<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <link rel="icon" type="image/png" href="{{ asset('wp-content/bower_components/greefair/img/logo.png') }}"/>

        <title>{{env('APP_NAME')}}</title>

        <!--    Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

        <!--Fontawesom-->
        <link rel="stylesheet" href="{{ asset('wp-content/bower_components/greefair/font-awesome/css/font-awesome.min.css') }}">

        <!--Animated CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('wp-content/bower_components/greefair/css/animate.min.css') }}">

        <!-- Bootstrap -->
        <link href="{{ asset('wp-content/bower_components/greefair/css/bootstrap.min.css') }}" rel="stylesheet">
        <!--Bootstrap Carousel-->
        <link type="text/css" rel="stylesheet" href="{{ asset('wp-content/bower_components/greefair/css/carousel.css')}}"/>

        <link rel="stylesheet" href="{{ asset('wp-content/bower_components/greefair/css/isotope/style.css') }}">

        <!--Main Stylesheet-->
        <link href="{{ asset('wp-content/bower_components/greefair/css/style.css') }}" rel="stylesheet">
        <!--Responsive Framework-->
        <link href="{{ asset('wp-content/bower_components/greefair/css/responsive.css') }}" rel="stylesheet">
        <!-- LightBox -->
        <link rel="stylesheet" href="{{asset('wp-content/plugins/lightbox/ekko-lightbox.css')}}">
        {{-- whatsapp --}}
        <link href="{{ asset('wp-content/plugins/wpp/floating-wpp.min.css') }}" rel="stylesheet" />


        <style type="text/css">
            .scrollspy-example {
                position: relative;
                height: 200px;
                overflow: scroll;
            }
            iframe {
                width: 100%;
            }
        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body data-spy="scroll" data-target="#list-nav" class="scrollspy-example">
        <div id="whatsapp" style="z-index: 1000"></div>

        <!--Start Hedaer Section-->
        <section id="header">
            <div class="header-area">
                <div class="top_header">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 zero_mp">
                                <div class="address">
                                    <i class="fa fa-home floatleft"></i>
                                    <p>{{ $home->address ?? ""}}</p>
                                </div>
                            </div>
                            <!--End of col-md-4-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 zero_mp">
                                <div class="phone">
                                    <i class="fa fa-phone floatleft"></i>
                                    <p>{{ $home->phone ?? ""}}</p>
                                </div>
                            </div>
                        </div>
                        <!--End of row-->
                    </div>
                    <!--End of container-->
                </div>
                <!--End of top header-->
                <div class="header_menu text-center" data-spy="affix" data-offset-top="50" id="nav">
                    <div class="container">
                        <nav class="navbar navbar-default zero_mp ">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand custom_navbar-brand" href="#"><img src="{{ asset('wp-content/bower_components/greefair/img/logo.png') }}" alt=""></a>
                            </div>
                            <!--End of navbar-header-->

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse zero_mp" id="bs-example-navbar-collapse-1">
                                <ul id="list-nav" class="nav navbar-nav navbar-right main_menu">
                                    <li class="active"><a href="#header">Home <span class="sr-only">(current)</span></a></li>
                                    <li><a href="#welcome">Tentang Kami</a></li>
                                    <li><a href="#testimonial">Pengurus</a></li>
                                    <li><a href="#members">Anggota</a></li>
                                    <li><a href="#blog">Agenda</a></li>
                                    <li><a href="#portfolio">Gallery</a></li>
                                    <li><a href="#contact">Pendaftaran</a></li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                        <!--End of nav-->
                    </div>
                    <!--End of container-->
                </div>
                <!--End of header menu-->
            </div>
            <!--end of header area-->
        </section>
        <!--End of Hedaer Section-->



        <!--Start of slider section-->
        <section id="slider">
            <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="slider_overlay">
                            <img src="{{ asset('wp-content/bower_components/greefair/img/img1.jpg') }}" alt="...">
                            <div class="carousel-caption">
                                <div class="slider_text">
                                    <h3>{{$home->title ?? ""}}</h3>
                                    <h2>{{$home->subtitle ?? ""}}</h2>
                                    <p>{{$home->description ?? ""}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of item With Active-->
                    <div class="item">
                        <div class="slider_overlay">
                            <img src="{{ asset('wp-content/bower_components/greefair/img/img2.jpg') }}" alt="...">
                            <div class="carousel-caption">
                                <div class="slider_text">
                                    <h3>{{$home->title ?? ""}}</h3>
                                    <h2>{{$home->subtitle ?? ""}}</h2>
                                    <p>{{$home->description ?? ""}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of Item-->
                    <div class="item">
                        <div class="slider_overlay">
                            <img src="{{ asset('wp-content/bower_components/greefair/img/img3.jpg') }}" alt="...">
                            <div class="carousel-caption">
                                <div class="slider_text">
                                    <h3>{{$home->title ?? ""}}</h3>
                                    <h2>{{$home->subtitle ?? ""}}</h2>
                                    <p>{{$home->description ?? ""}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of item-->
                </div>
                <!--End of Carousel Inner-->
            </div>
        </section>
        <!--end of slider section-->



        <!--Start of welcome section-->
        <section id="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wel_header">
                            <h2>{{ $home->title_about ?? "selamat datang"}}</h2>
                            <p>{{ $home->subtitle_about ?? ""}}</p>
                        </div>
                    </div>
                </div>
                <!--End of row-->
                <div class="row">
                    @if(isset($about) && $about)
                    @foreach($about as $key => $val)
                    <div class="col-md-3">
                        <div class="item">
                            <a data-toggle="modal" data-target="#myModal-{{$key}}">
                                <div class="single_item">
                                    <div class="item_list">
                                        <div class="welcome_icon">
                                            <i class="{{ $val->icon ?? 'fa fa-leaf' }}"></i>
                                        </div>
                                        <h4>{{$val->title}}</h4>
                                        <p>{{$val->subtitle}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal-{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:10000">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Deskripsi</h4>
                          </div>
                          <div class="modal-body">
                            {!! $val->description ?? "" !!}
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    @endif
                </div>
                <!--End of row-->
            </div>
            <!--End of container-->
        </section>

        <!--Start of testimonial-->
        <section id="testimonial">
            <div class="testimonial_overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="testimonial_header text-center">
                                <h2>{{ $home->title_team ?? "pengurus"}}</h2>
                                <p>{{ $home->subtitle_team ?? ""}}</p>
                            </div>
                        </div>
                    </div>
                    <!--End of row-->
                    <section id="carousel">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                                        <!-- Carousel indicators -->
                                        @if(isset($team) && $team)
                                        <ol class="carousel-indicators">
                                            @foreach ($team as $key => $val)
                                                <li data-target="#fade-quote-carousel" data-slide-to="{{$key}}" class="{{$key == 0 ? 'active' : ''}}"></li>
                                            @endforeach
                                        </ol>
                                        <div class="carousel-inner">
                                        <!-- Carousel items -->
                                        @foreach ($team as $key => $val)

                                            <div class="{{$key == 0 ? 'active' : ''}} item">
                                                <div class="row">
                                                    @foreach($val as $kk => $value)
                                                    <div class="col-md-6">
                                                        <div class="profile-circle">
                                                            <img src="{{url('')}}/{{$active['team_path']}}/{{$value->image ?? ''}}" alt="">
                                                        </div>
                                                        <div class="testimonial_content">
                                                            <i class="fa fa-quote-left"></i>
                                                            {!!$value->description!!}
                                                        </div>
                                                        <div class="testimonial_author">
                                                            <h5>{{$value->name}}</h5>
                                                            <p>{{$value->title}}</p>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End of row-->
                        </div>
                        <!--End of container-->
                    </section>
                    <!--End of carousel-->
                </div>
            </div>
            <!--End of container-->
        </section>
        <!--end of testimonial-->

        <!--Start of members-->
        <section id="members">
            <div class="members_overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="members_header text-center">
                                <h2>{{ $home->title_members ?? "Anggota"}}</h2>
                                <p>{{ $home->subtitle_members ?? ""}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">Nama</th>
                                      <th scope="col">Foto</th>
                                      <th scope="col">KTA</th>
                                      <th scope="col">Instansi</th>
                                    </tr>
                                  </thead>
                                <tbody>
                                    @if($members)
                                        @foreach ($members as $key => $val)
                                            <tr>
                                              <th scope="row">{{++$key}}</th>
                                              <td>
                                                  @if ($val->name_url)
                                                      <a href="{{$val->name_url}}" target="_blank" rel="noreferrer">
                                                          {{$val->name}}
                                                      </a>
                                                  @else
                                                      {{$val->name}}
                                                  @endif

                                              </td>
                                              <td>
                                                  <a data-toggle="lightbox" href="{{url('')}}/{{$active['members_path']}}/{{$val->image ?? ''}}">
                                                      Klik untuk melihat foto
                                                  </a>
                                              </td>
                                              <td>
                                                  @if ($val->kta_url)
                                                  <a href="{{$val->kta_url}}" target="_blank" rel="noreferrer">
                                                      {{$val->kta}}
                                                  </a>
                                                  @else
                                                      {{$val->kta}}
                                                  @endif
                                              </td>
                                              <td>{{$val->instansi}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!--End of container-->
        </section>
        <!--end of members-->

        <!--Start of blog-->
        <section id="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="latest_blog text-center">
                            <h2>{{ $home->title_agenda ?? "agenda"}}</h2>
                            <p>{{ $home->subtitle_agenda ?? ""}}</p>
                        </div>
                    </div>
                </div>
                <!--End of row-->
                <div class="row">
                    @if(isset($agenda) && $agenda)
                    @foreach($agenda as $key => $value)
                    <div class="col-md-4">
                        <div class="blog_news">
                            <div class="single_blog_item">
                                <div class="blog_img">
                                    <img src="{{url('')}}/{{$active['agenda_path']}}/{{$value->image ?? ''}}" alt="">
                                </div>
                                <div class="blog_content">
                                    <h3>{{$value->title}}</h3>

                                    <p class="blog_news_content">
                                        {!!$value->description ?? ""!!}
                                    </p>
                                    <br>
                                    <a href="{{url('')}}/{{$active['agenda_path']}}/{{$value->file ?? ''}}" download="" class="blog_link">
                                        Download <i class="fa fa-download"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
                <!--End of row-->
            </div>
            <!--End of container-->
        </section>
        <!-- end of blog-->

        <!--Start of portfolio-->
        <section id="portfolio" class="text-center">
            <div class="col-md-12">
                <div class="portfolio_title">
                    <h2>{{ $home->title_gallery ?? "galeri"}}</h2>
                    <p>{{ $home->subtitle_gallery ?? ""}}</p>
                </div>
            </div>
            <!--End of col-md-2-->
            <div class="colum">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="notes">
                                @if(isset($gallery) && $gallery)
                                @foreach($gallery as $key => $value)
                                <div class="note blue">
                                    <a data-toggle="lightbox" href="{{url('')}}/{{$active['gallery_path']}}/{{$value->image ?? ''}}">
                                        <div class="img_overlay">
                                            <p>{{$value->title}}</p>
                                        </div>
                                    </a>
                                    <img src="{{url('')}}/{{$active['gallery_path']}}/{{$value->image ?? ''}}" alt="">
                                </div>
                                @endforeach
                                @endif
                            </div>
                            <!--End of notes-->
                        </div>
                        <!--End of col-lg-12-->
                    </div>
                    <!--End of row-->
                </div>
                <!--End of container-->
            </div>
            <!--End of colum-->
        </section>
        <!--end of portfolio-->

        <!--Start of contact-->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="colmd-12">
                        <div class="contact_area text-center">
                            <h3>{{ $home->title_registration ?? "Pendaftaran"}}</h3>
                            <p>{{ $home->subtitle_registration ?? ""}}</p>
                        </div>
                    </div>
                </div>
                <!--End of row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="office">
                            <div class="title">
                                <h5>Alamat</h5>
                            </div>
                            <div class="office_location">
                                <div class="address">
                                    <i class="fa fa-map-marker"><span>{{ $home->address ?? ""}}</span></i>
                                </div>
                                <div class="phone">
                                    <i class="fa fa-phone"><span>{{ $home->phone ?? ""}}</span></i>
                                </div>
                                <div class="whatsapp">
                                    <a href="https://api.whatsapp.com/send?phone={!!$home->whatsapp ?? ""!!}" target="_blank">
                                        <i class="fa fa-comment"><span>{!!$home->whatsapp ?? ""!!}</span></i>
                                    </a>
                                </div>
                                <div class="email">
                                    <i class="fa fa-envelope"><span>{{ $home->email ?? ""}}</span></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="msg">
                            <div class="msg_title">
                                <h5>
                                    {{$registration->title ?? ""}} &nbsp
                                    <a href="javascript:;" data-toggle="modal" data-target="#modal-reg" title="Cara Pendaftaran">
                                        <i class="fa fa-book text-info"></i>
                                    </a>
                                </h5>
                                <div class="modal fade" id="modal-reg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:10000">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Cara Pendaftaran</h4>
                                      </div>
                                      <div class="modal-body" style="padding: 15px 35px">
                                        {!!$registration->description ?? ""!!}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="form_area">
                                <!-- CONTACT FORM -->
                                {!!$registration->form ?? ""!!}
                            </div>
                        </div>
                    </div>
                    <!--End of col-md-6-->
                </div>
                <!--End of row-->
            </div>
            <!--End of container-->
        </section>
        <!--End of contact-->



        <!--Start of footer-->
        <section id="footer">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-6">
                        <div class="copyright">
                            <p>Dosen Kolaborasi Lintas Perguruan Tinggi {{"@".date('Y')}}</p>
                        </div>
                    </div>
                </div>
                <!--End of row-->
            </div>
            <!--End of container-->
        </section>
        <!--End of footer-->



        <!--Scroll to top-->
        <a href="#" id="back-to-top" title="Back to top">&uarr;</a>


        <!--End of Scroll to top-->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>-->
        <script src="{{ asset('wp-content/bower_components/greefair/js/jquery-1.12.3.min.js') }}"></script>

        <!--Isotope-->
        <script src="{{ asset('wp-content/bower_components/greefair/js/isotope/min/scripts-min.js') }}"></script>
        {{-- <script src="{{ asset('wp-content/bower_components/greefair/js/isotope/cells-by-row.js') }}"></script> --}}
        <script src="{{ asset('wp-content/bower_components/greefair/js/isotope/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('wp-content/bower_components/greefair/js/isotope/packery-mode.pkgd.min.js') }}"></script>
        <script src="{{ asset('wp-content/bower_components/greefair/js/isotope/scripts.js') }}"></script>


        <!--Back To Top-->
        <script src="{{ asset('wp-content/bower_components/greefair/js/backtotop.js') }}"></script>


        <!--JQuery Click to Scroll down with Menu-->
        <script src="{{ asset('wp-content/bower_components/greefair/js/smooth-scroll-bootstrap-scrollspy/jquery.smoothScroll.min.js') }}"></script>
        <!--WOW With Animation-->
        <script src="{{ asset('wp-content/bower_components/greefair/js/wow.min.js') }}"></script>
        <!-- LightBox -->
        <script src="{{asset('wp-content/plugins/lightbox/ekko-lightbox.min.js')}}"></script>
        {{-- whatsapp --}}
        <script src="{{asset('wp-content/plugins/wpp/floating-wpp.min.js')}}"></script>
        <script>
            new WOW().init();

            $(function() {
                $('.scrollspy-example').smoothScroll();
            });
            $('#list-nav li').click(function () {
                $('.main_menu li').removeClass('active');
                $(this).addClass('active');
            });
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
            $(function () {
            $('#whatsapp').floatingWhatsApp({
              phone: '{{$home->whatsapp ?? ''}}',
              position: 'right'
            });
          });
        </script>


        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('wp-content/bower_components/greefair/js/bootstrap.min.js') }}"></script>
        <!-- Custom JavaScript-->
        <script src="{{ asset('wp-content/bower_components/greefair/js/main.js') }}"></script>
    </body>

</html>
