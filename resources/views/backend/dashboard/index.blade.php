@extends('layouts.backend')
@section('title')
  {{ucwords($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucwords($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
          @if (Session::has('msg'))
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center>
              <strong>{{ session('msg') }}</strong>
            </center>
          </div>
          @endif
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">{{ucwords($active['title'])}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="title" placeholder="Judul" value="{{$data->title ?? old('title')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle" class="col-sm-2 control-label">Sub Judul</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle" class="form-control" id="subtitle" placeholder="Sub Judul" value="{{$data->subtitle ?? old('subtitle')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="text" name="email" class="form-control" id="email" placeholder="Email" value="{{$data->email ?? old('email')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Alamat</label>

                  <div class="col-sm-10">
                    <input type="text" name="address" class="form-control" id="address" placeholder="Alamat" value="{{$data->address ?? old('address')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Telepon</label>

                  <div class="col-sm-10">
                    <input type="text" name="phone" class="form-control" id="phone" placeholder="Telepon" value="{{$data->phone ?? old('phone')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="whatsapp" class="col-sm-2 control-label">WhatsApp</label>

                  <div class="col-sm-10">
                    <input type="text" name="whatsapp" class="form-control" id="whatsapp" placeholder="WhatsApp" value="{{$data->whatsapp ?? old('whatsapp')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <input type="text" name="description" class="form-control" id="description" placeholder="Deskripsi" value="{{$data->description ?? old('description')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_about" class="col-sm-2 control-label">Judul Tentang Kami</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_about" class="form-control" id="title_about" placeholder="Judul Tentang Kami" value="{{$data->title_about ?? old('title_about')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle_about" class="col-sm-2 control-label">Sub Judul Tentang Kami</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle_about" class="form-control" id="subtitle_about" placeholder="Sub Judul Tentang Kami" value="{{$data->subtitle_about ?? old('subtitle_about')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_team" class="col-sm-2 control-label">Judul Pengurus</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_team" class="form-control" id="title_team" placeholder="Judul Pengurus" value="{{$data->title_team ?? old('title_team')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle_team" class="col-sm-2 control-label">Sub Judul Pengurus</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle_team" class="form-control" id="subtitle_team" placeholder="Sub Judul Pengurus" value="{{$data->subtitle_team ?? old('subtitle_team')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_members" class="col-sm-2 control-label">Judul Anggota</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_members" class="form-control" id="title_members" placeholder="Judul Anggota" value="{{$data->title_members ?? old('title_members')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle_members" class="col-sm-2 control-label">Sub Judul Anggota</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle_members" class="form-control" id="subtitle_members" placeholder="Sub Judul Anggota" value="{{$data->subtitle_members ?? old('subtitle_members')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_agenda" class="col-sm-2 control-label">Judul Agenda</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_agenda" class="form-control" id="title_agenda" placeholder="Judul Agenda" value="{{$data->title_agenda ?? old('title_agenda')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle_agenda" class="col-sm-2 control-label">Sub Judul Agenda</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle_agenda" class="form-control" id="subtitle_agenda" placeholder="Sub Judul Agenda" value="{{$data->subtitle_agenda ?? old('subtitle_agenda')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_gallery" class="col-sm-2 control-label">Judul Galeri</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_gallery" class="form-control" id="title_gallery" placeholder="Judul Galeri" value="{{$data->title_gallery ?? old('title_gallery')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle_gallery" class="col-sm-2 control-label">Sub Judul Galeri</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle_gallery" class="form-control" id="subtitle_gallery" placeholder="Sub Judul Galeri" value="{{$data->subtitle_gallery ?? old('subtitle_gallery')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_registration" class="col-sm-2 control-label">Judul Pendaftaran</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_registration" class="form-control" id="title_registration" placeholder="Judul Pendaftaran" value="{{$data->title_registration ?? old('title_registration')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle_registration" class="col-sm-2 control-label">Sub Judul Pendaftaran</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle_registration" class="form-control" id="subtitle_registration" placeholder="Sub Judul Pendaftaran" value="{{$data->subtitle_registration ?? old('subtitle_registration')}}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
@endsection
@push('appjs')
<script>
  $(function () {
    const calculation = (is_auto) => {
      if (is_auto == 1) {
        $('#hide-score').hide();
        $('input[name=score]').val(0);
      }else{
        $('#hide-score').show();
      }
    }
    $('#calculation').change(function(e) {
      calculation($(this).val());
    });
    @if (isset($data->is_auto))
      calculation({{$data->is_auto}});
    @endif
  })
</script>
@endpush
