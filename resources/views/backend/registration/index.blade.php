@extends('layouts.backend')
@section('title')
  {{ucwords($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucwords($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
          @if (Session::has('msg'))
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center>
              <strong>{{ session('msg') }}</strong>
            </center>
          </div>
          @endif
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">{{ucwords($active['title'])}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="title" placeholder="Judul" value="{{$data->title ?? old('title')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="form" class="col-sm-2 control-label">Form</label>

                  <div class="col-sm-10">
                    <input type="text" name="form" class="form-control" id="form" placeholder="Form" value="{{$data->form ?? old('form')}}">
                    <span class="text-danger">
                        Masukkan link google form
                    </span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Cara Pendaftaran</label>
                  <div class="col-sm-10">
                    <textarea name="description" class="textarea-wysi"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$data->description ?? ''}}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
@endsection
@push('appjs')
<script>
  $(function () {
    const calculation = (is_auto) => {
      if (is_auto == 1) {
        $('#hide-score').hide();
        $('input[name=score]').val(0);
      }else{
        $('#hide-score').show();
      }
    }
    $('#calculation').change(function(e) {
      calculation($(this).val());
    });
    @if (isset($data->is_auto))
      calculation({{$data->is_auto}});
    @endif
  })
</script>
@endpush