@extends('layouts.backend')
@section('title')
  {{ucwords($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucwords($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">{{ucwords($active['title'])}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{$data->id ?? 0}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="title" placeholder="Judul" required="" value="{{$data->title ?? old('title')}}">
                  </div>
                </div>
               
                <div class="form-group">
                  <label for="image" class="col-sm-2 control-label">Foto</label>
                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="Foto" placeholder="Foto">
                    <input type="hidden" name="old_image" value="{{$data->image ?? ''}}">
                    @if(isset($data->image) && $data->image)
                      <a data-toggle="lightbox" href="{{url('')}}/{{$active['download_path']}}/{{$data->image ?? ''}}">
                        Lihat Gambar
                      </a>
                    @endif
                  </div>
                </div>
               
                <div class="form-group">
                  <label for="file" class="col-sm-2 control-label">File</label>
                  <div class="col-sm-10">
                    <input type="file" name="file" class="form-control" id="file" placeholder="File">
                    <input type="hidden" name="old_file" value="{{$data->file ?? ''}}">
                    @if(isset($data->file) && $data->file)
                      <a download="true" href="{{url('')}}/{{$active['download_path']}}/{{$data->file ?? ''}}">
                        Download File
                      </a>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <textarea name="description" class="textarea-wysi"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$data->description ?? ''}}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Batal</a>
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    
@endsection