@extends('layouts.backend')
@section('title')
  {{ucwords($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucwords($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">{{ucwords($active['title'])}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{$data->id ?? 0}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Nama" required="" value="{{$data->name ?? old('name')}}">
                  </div>
                </div>
                <div class="form-group">
                    <label for="name_url" class="col-sm-2 control-label">URL Forlap</label>

                    <div class="col-sm-10">
                      <input type="text" name="name_url" class="form-control" id="name_url" placeholder="URL Forlap" value="{{$data->name_url ?? old('name_url')}}">
                    </div>
                </div>
                <div class="form-group">
                  <label for="image" class="col-sm-2 control-label">Foto</label>
                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="Foto" placeholder="Foto">
                    <input type="hidden" name="old_image" value="{{$data->image ?? ''}}">
                    @if(isset($data->image) && $data->image)
                      <a data-toggle="lightbox" href="{{url('')}}/{{$active['download_path']}}/{{$data->image ?? ''}}">
                        Lihat Gambar
                      </a>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                    <label for="kta" class="col-sm-2 control-label">No KTA</label>
                    <div class="col-sm-10">
                      <input type="text" name="kta" class="form-control" id="kta" placeholder="KTA" value="{{$data->kta ?? old('kta')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="kta_url" class="col-sm-2 control-label">KTA link</label>
                    <div class="col-sm-10">
                      <input type="text" name="kta_url" class="form-control" id="kta_url" placeholder="KTA Link" value="{{$data->kta_url ?? old('kta_url')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="instansi" class="col-sm-2 control-label">Instansi</label>
                    <div class="col-sm-10">
                      <input type="text" name="instansi" class="form-control" id="instansi" placeholder="Instansi" value="{{$data->instansi ?? old('instansi')}}">
                    </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Batal</a>
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

@endsection
