@extends('layouts.backend')
@section('title')
  {{ucwords($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucwords($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
          @if (Session::has('msg'))
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center>
              <strong>{{ session('msg') }}</strong>
            </center>
          </div>
          @endif
          @if($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <center>
                  <strong>{{ implode('', $errors->all('<div>:message</div>')) }}</strong>
                </center>
              </div>
          @endif
        	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabel {{ucwords($active['title'])}}</h3>
              <a href="{{$active['url']}}/create" type="button" class="btn btn-primary btn-xs pull-right">
              	<i class="fa fa-plus"></i> Tambah
              </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="dt-1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Image</th>
                  <th>No KTA</th>
                  <th>Instansi</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data) && $data)
                  @foreach($data as $idx => $row)
                    <tr>
                      <td>{{++$idx}}</td>
                      <td>
                          {{$row->name}}
                          @if(isset($row->name_url) && $row->name_url)
                            &nbsp
                            <a href="{{$row->name_url}}" target="_blank" rel="noreferrer">
                                <i class="fa fa-external-link-alt"></i>
                            </a>
                          @endif
                      </td>
                      <td>
                        <a data-toggle="lightbox" href="{{url('')}}/{{$active['download_path']}}/{{$row->image ?? ''}}">
                          Show Image
                        </a>
                      </td>
                      <td>
                          {!!$row->kta!!}
                          @if (isset($row->kta_url) && $row->kta_url)
                            &nbsp
                            <a href="{{$row->kta_url}}" target="_blank" rel="noreferrer">
                                <i class="fa fa-external-link-alt"></i>
                            </a>
                          @endif
                      </td>
                      <td>{!!$row->instansi!!}</td>
                      <td>
                        <center>
                          <a href="{{url($active['url'])}}/{{$row->id}}/edit"><i class="fa fa-edit"></i></a>
                          &nbsp
                          <a href="javascript:;" onclick="deleteData({{$row->id}})"><i class="fa fa-trash"></i></a>
                        </center>
                      </td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    <form action="" method="post" id="form-delete">
        @csrf
        @method('delete')
    </form>
@endsection
@push('appjs')
<script>
  $(function () {
    $('#dt-1').DataTable()
  });
  const deleteData = (id) => {
      swal({
        title: "Delete",
        text: "Are you sure to delete this data?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $(`#form-delete`).attr('action', `{{$active['url']}}/${id}`);
          $(`#form-delete`).submit();
        }
      });
    }
</script>
@endpush
