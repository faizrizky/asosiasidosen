<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->prefix('adminpanel')->group(function () {
	Route::get('/', function () {
	    return redirect('adminpanel/dashboard');
	});
	Route::get('/dashboard', [App\Http\Controllers\Be\DashboardController::class, 'index']);
	Route::post('/dashboard', [App\Http\Controllers\Be\DashboardController::class, 'store']);
	Route::resource('/about', App\Http\Controllers\Be\AboutController::class);
	Route::resource('/agenda', App\Http\Controllers\Be\AgendaController::class);
	Route::get('/change-password', [App\Http\Controllers\Be\PasswordController::class, 'index']);
	Route::post('/change-password', [App\Http\Controllers\Be\PasswordController::class, 'store']);
	Route::resource('/gallery', App\Http\Controllers\Be\GalleryController::class);
	Route::resource('/members', App\Http\Controllers\Be\MembersController::class);
	Route::resource('/team', App\Http\Controllers\Be\TeamController::class);
	Route::get('/registration', [App\Http\Controllers\Be\RegistrationController::class, 'index']);
	Route::post('/registration', [App\Http\Controllers\Be\RegistrationController::class, 'store']);
});

Route::get('/', [App\Http\Controllers\Fe\HomeController::class,'index']);
Route::post('/', [App\Http\Controllers\Fe\HomeController::class,'store']);
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
